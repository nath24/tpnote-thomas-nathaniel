import React from "react";
import "react-router-dom";
import "../style/card.css";

function Logement() {
    return (
      <div className="wrapper">
        <Card
            img="https://images.unsplash.com/photo-1597648208284-2e984df54689?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fG1haXNvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=400&q=80"
            title="    Maison Traditionnelle"
            description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
  
        <Card
          img="https://images.unsplash.com/photo-1585240619493-20445877685f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
  
        <Card
          img="https://images.unsplash.com/photo-1585240619493-20445877685f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
        <Card
          img="https://images.unsplash.com/photo-1620718024338-112b5a0764e7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8bWFpc29ufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
  
        <Card
          img="https://images.unsplash.com/photo-1614291486355-ba4fc328156b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fG1haXNvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
  
        <Card
          img="https://images.unsplash.com/photo-1614291486355-ba4fc328156b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fG1haXNvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />

        <Card
          img="https://images.unsplash.com/photo-1597648208284-2e984df54689?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fG1haXNvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />

        <Card
          img="https://images.unsplash.com/photo-1614291486355-ba4fc328156b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fG1haXNvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=400&q=80"
          title="Maison Traditionnelle"
          description="Cette maison se situe à la résidence de la famille de la ville de Paris. Elle est située à la résidence de la famille de la ville de Paris."
        />
      </div>
    );
    
  }
  function Card(props) {
    return (
      <div className="card">
        <div className="card__body">
          <img src={props.img} class="card__image" />
          <h2 className="card__title">{props.title}</h2>
          <p className="card__description">{props.description}</p>
        </div>
        <button className="card__btn">En savoir plus</button>
      </div>
    );
  }
    export default Logement;