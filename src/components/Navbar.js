import { Link } from "react-router-dom";
import "../style/Navbar.css";

function Header() {
  return (
    <header>
      <nav className="Nav">
        <Link to="/">Home</Link>
        <Link to="/Search">Recherche</Link>
        <Link to="/Faq">FAQ</Link>
      </nav>
    </header>
  );
}

export default Header;
