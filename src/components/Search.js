import React from "react";
import SearchBar from "../SearchBar";
import Table from "../Table";
import { Box, Heading } from "grommet";
import Records from "../Data/records.js";

function Recherche() {
  const [result, setResult] = React.useState([]);
  const [searchOn, setSearchOn] = React.useState(false);
  return (
    <Box direction={"column"}>
      <Box alignSelf={"center"} background={"#blue"} fill={"horizontal"}>
        <Heading alignSelf={"center"} color={"#000000"}>
          Recherchez votre logement
        </Heading>
        <SearchBar
          setResult={setResult}
          data={Records}
          searchKeys={[
            "first_name",
            "company_name",
            "country",
            "adress",
            "city",
          ]}
          title={"Search for Name, Price or Rue"}
          setSearchActive={setSearchOn}
        />
      </Box>
      <Box direction={"row"} background={"#0000"} gap={"medium"}>
        <Table tableHeading={"Résultas"} data={result} />
      </Box>
    </Box>
  );
}

export default Recherche;
