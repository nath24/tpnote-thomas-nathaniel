import * as React from "react";
import { Routes, Route } from "react-router-dom";
import Recherche from "./components/Search";
import Header from "./components/Navbar";
import Logement from "./components/card";
import Faq from "./components/Faq";
import Error from "./components/404";

function App() {
  return (
    <div className="App">
      <Header> </Header>
      <Routes>
        <Route path="/" element={<Logement />} />
        <Route path="/Search" element={<Recherche />} />
        <Route path="/Faq" element={<Faq />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  );
}

export default App;
