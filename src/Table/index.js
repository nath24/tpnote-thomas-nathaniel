import { DataTable, Box, Heading } from "grommet/es6";
import React from "react";
import { DataTableColumns } from "./DataTabelColumns";

const Table = ({ data, tableHeading }) => {
  return (
    <Box pad={"small"}>
      <Heading alignSelf={"center"} level={"2"} color={"black"}>
        {tableHeading}
      </Heading>
      <DataTable data={data} columns={DataTableColumns} size={"medium"} />
    </Box>
  );
};

export default Table;
