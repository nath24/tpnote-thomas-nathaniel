import React from "react";
import { Text } from "grommet";

export const DataTableColumns = [
  {
    property: "first_name",
    header: <Text weight={"bold"}>First Name</Text>,
  },
  {
    property: "nom_rue",
    header: <Text weight={"bold"}>Nom de la rue</Text>,
  },
  {
    property: "country",
    header: <Text weight={"bold"}>Country</Text>,
  },
  {
    property: "address",
    header: <Text weight={"bold"}>Adresse</Text>,
  },
  {
    property: "city",
    header: <Text weight={"bold"}>Ville</Text>,
  },
];
